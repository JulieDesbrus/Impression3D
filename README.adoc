// ---
// layout: master
// title: Présentation de L'impression 3D
// :backend: deckjs
// ---
:revealjs_mouseWheel: true
:revealjsdir: /reveal.js
:revealjs_history: true
:revealjs_hideAddressBar: true
:revealjs_defaultTiming : 120
:icons: font
:imagesdir: ./Images
:source-highlighter: highlightjs
:theme: jekyll-theme-hacker
:revealjs_theme: white
:customcss: /css/Open.css

= L'impression 3D


== L'impression 3D

image:Mendel90_Dibond.jpg["Imprimante 3D",400,500]

== Présentation

Charles de MAGNEVAL +
image:/images/logoOpen.png["Open",float="right"] +
Charles.de_magneval@open-groupe.com +
v1.0, 2017-07-13 +
 +
https://github.com/magneval/
https://gitlab.com/magneval/


=== Plan

[%step]
* Les techniques de fabrication
* La conception et la modélisation des pièces à réaliser
* Les différents matériaux d’impression
* Les formats d’échanges
* La préparation des pièces pour impression
* Le slicing

== Les techniques de fabrication

=== Les techniques de fabrication
==== par déformation de matière

[%step]
* La forge
* L'emboutissage
* Le thermoformage
* Le pliage

=== Les techniques de fabrication
==== par supression de matière

* L'usinage
[%step]
** Le tournage
** Le fraisage
** L'électro érosion

=== Les techniques de fabrication
==== par supression de matière

* La découpe
[%step]
** au laser
** au plasma
** au jet d'eau
** l'oxycoupage

=== Les techniques de fabrication
==== par assemblage de matière

* La soudure
[%step]
** a l'étain
** a oxyacétylène
** a l'arc
** électrique

=== Les techniques de fabrication
==== par assemblage de matière

[%step]
* Le collage
* Le rivettage
* Le boulonnage
* L'agrafage
* L'emboîtement

=== Les techniques de fabrication
==== par ajout de matière

* Le moulage
[%step]
** à la cire fondue
** metallique
** roto-moulage

[NOTE.speaker]
--
--

=== Les techniques de fabrication
==== par ajout de matière

* par dépot de matière
[%step]
** par polymérisation
** laminaire
** par fritage
** par dépôt de fil fondu

[NOTE.speaker]
--
Stréréolithographie
dépot de papier
date des années 80
--


=== Les techniques de fabrication
==== par dépôt de fil fondu

* avantages
[%step]
** simplicité
** créativité
** accessibilité

=== Les techniques de fabrication
==== par dépôt de fil fondu

* inconvénients
[%step]
** durée de réalisation
** fragilité
** les matériaux
** fiabilité

[NOTE.speaker]
--
prévoir de conjuguer les techniques

--

== La conception et la modélisation des pièces à réaliser

=== La conception et la modélisation des pièces à réaliser
==== les contraintes

[%step]
* le point fort de l'impressions 3D est le peu de contraintes
* sauf :

=== les contraintes
[%step]
* le temps d'impression
* le collage au plateau
* la déformation pendant l'impression
* la taille des pièces imprimables

[NOTE.speaker]
--
parler du risque de plantages
* logiciel
* pb de fils
* …
--


=== les contraintes
* La forme des pièces
** pas trop de devers
** besoin de support
** dimension minimum
* assemblage impossible

[NOTE.speaker]
--
--


=== les contraintes

exemple lettres
[%step]
* T
* Y
* M
* H
* O



=== La conception et la modélisations des pièces à réaliser
==== les logiciels

* débutants
[%step]
** tinkercad
** freecad
** sketchup

[NOTE.speaker]
--
https://www.tinkercad.com[tinkercad^]
--

=== les logiciels

image:Capture_d_écran_2018-07-08_21.19.47.png["tinkercad",960,540]

=== La conception et la modélisation des pièces à réaliser
==== les logiciels

* artistiques
[%step]
** blender
** sculptris

[NOTE.speaker]
--
https://grid.space/kiri[Kiri:Moto^]
--

=== La conception et la modélisation des pièces à réaliser
==== les logiciels

* Open sources
[%step]
** OpenSCAD
** OpenJSCAD
** Shapesmith

[NOTE.speaker]
--
http://openscad.net[OpenSCAD^]
https://openjscad.org[OpenJSCAD^]
https://shapesmith.net[Shapesmith^]
--

=== La conception et la modélisation des pièces à réaliser
==== les logiciels

* industriels
[%step]
** fusion 360
** Solid Works
** Onshape

[NOTE.speaker]
--
https://cad.onshape.com/signin[Onshape^]
--

== Les différents matériaux d’impression

=== Les différents matériaux d’impression
* à base de polymère
* le PLA
* l'ABS
* le PET
* le nylon
* le tpu
* ...

[NOTE.speaker]
--
Les matériaux
https://www.filimprimante3d.fr/content/47-pla-abs-petg-choisir-son-materiau-pour-imprimante-3d-en-fonction-de-l-application[matériaux^]
--

== Les formats d’échange

=== Les formats d’échange
==== de dessin :
* DXF
* SVG
* …

=== Les formats d’échange
==== de CAO

* DWG
* Parasolid
* …

=== Les formats d’échange
==== pour l'impression

* STL
* G-Code
* …

[NOTE.speaker]
--
https://www.viewstl.com[STL^]
http://jherrm.com/gcode-viewer/[G-Code^]
--

== La préparation des pièces pour impression


=== La préparation des pièces pour impression

[%step]
* la diposition des pièces sur le plateau
* l'orientation pour une meilleure impression
* la mise en place de support
* l'utilisation d'une base

=== La préparation des pièces pour impression

* paramétrage de la machine cible
[%step]
** dimensions
** plateau chaufant
** ventilateur
** double buse

=== La préparation des pièces pour impression

* paramétrage du materiau
[%step]
** température
** profil de chauffe
** …

== Le slicing

=== Le slicing
==== définition
[%step]
* trancher la pièce en différentes couches. 
* définir la stratégie d'impression de la pièce. 
[%step]
** épaisseur de la coque. 
** densité de remplissage et structure (losanges, nids d'abeilles…)
** utilisation de support. 
** température d'impression. 

=== Le slicing
==== création d'un fichier interprétable par l'imprimante.
* fichier format GCODE.

=== Le slicing
==== les logiciels

[%step]
* cura
* repiter
* slic3r
* simplify3d
* Kiri:Moto


=== Le slicing

image:Capture_d_écran_2018-07-08_21.18.03.png["cura",960,540]

[NOTE.speaker]
--
https://grid.space/kiri[Kiri:Moto^]
--

== La préparation des pièces après impression


=== La préparation des pièces après impression

[%step]
* ponçage
* polissaqe
* point d'arrêt
* …

== démo

== des questions
